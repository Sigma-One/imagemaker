from datetime import datetime

logLevel = 0

def setLogLevel(level):
    global logLevel
    logLevel = level

def LOG_RAW(text, *tags):
    s = ""
    tagCtr = False
    for t in tags:
        if (tagCtr):
            s += "\033[100;30;1m"
        else:
            s += "\033[47;30;1m"
        tagCtr = not tagCtr
        s += str(t)
        s += "\033[0m"
    print(s + " " + str(text))

def LOG_DBUG(text, *tags):
    global logLevel
    if (logLevel <= 0):
        LOG_RAW(
            text,
            datetime.strftime(datetime.now(), "\033[44;30;1m %y/%m/%d %H:%M:%S \033[0m"),
            "\033[104;30;1m DBUG \033[0m",
            *tags
        )

def LOG_INFO(text, *tags):
    global logLevel
    if (logLevel <= 1):
        LOG_RAW(
            text,
            datetime.strftime(datetime.now(), "\033[42;30;1m %y/%m/%d %H:%M:%S \033[0m"),
            "\033[102;30;1m INFO \033[0m",
            ("\033[47;30;1m"+tags[0]+"\033[0m") if (len(tags) > 0) else "",
            *tags[1:]
        )

def LOG_WARN(text, *tags):
    global logLevel
    if (logLevel <= 2):
        LOG_RAW(
            text,
            datetime.strftime(datetime.now(), "\033[43;30;1m %y/%m/%d %H:%M:%S \033[0m"),
            "\033[103;30;1m WARN \033[0m",
            ("\033[47;30;1m" + tags[0] + "\033[0m") if (len(tags) > 0) else "",
            *tags[1:]
        )

def LOG_FAIL(text, *tags):
    global logLevel
    if (logLevel <= 3):
        LOG_RAW(
            text,
            datetime.strftime(datetime.now(), "\033[41;30;1m %y/%m/%d %H:%M:%S \033[0m"),
            "\033[101;30;1m FAIL \033[0m",
            ("\033[47;30;1m"+tags[0]+"\033[0m") if (len(tags) > 0) else "",
            *tags[1:]
        )
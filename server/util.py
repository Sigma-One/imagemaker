import struct

def getImageSize(path):
    with open(path, "rb") as imgFile:
        w, h = struct.unpack(">LL", imgFile.read(25)[16:24])
        return (int(w), int(h))
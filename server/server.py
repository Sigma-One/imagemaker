import logging
import os

import werkzeug
from flask import Flask
from flask import Response
from flask import render_template as renderTemplate
from flask import send_file       as sendFile
from flask import request
from flask import abort

from imghdr import what as imgType

from hashlib import sha3_256
from hashlib import shake_256

from functools import wraps

from log      import *
from template import *
from database import *

HOST = "0.0.0.0"
PORT = 8080

DATA_PATH = "../data"

TOKEN_EXPIRY = 30*24*60*60*60

app = Flask(
    __name__,
    template_folder="../layout",
    static_folder="../static"
)
log = logging.getLogger("werkzeug")
log.disabled = True
app.config["MAX_CONTENT_LENGTH"] = 16*1024*1024
app.config["SECRET_KEY"]         = "DEVKEY-DONOTUSE"

def login(username, password):
    uid = dbVerifyUserLogin(
        username,
        sha(password)
    )
    if not (uid):
        return None
    token = generateToken(uid)
    tokenDbSet(token, uid, expiry=TOKEN_EXPIRY)
    return {"uid" : uid, "token" : token}

def register(username, password):
    uid = dbCreateUser(
        username,
        sha(password)
    )
    if not (uid): return None
    return {"uid" : uid}

"""
Wraps a view function, adding request fields `uid` and `token`, getting them based on a token in the `auth` request header.
Will abort with 403 if the token is invalid or missing, otherwise proceeds with the view function
"""
def verifyToken(func):
    @wraps(func)
    def verifyTokenWrapper(*args, **kwargs):
        try:
            token = request.headers["auth"]
        except KeyError:
            abort(403)
        uid = uidFromToken(token)
        if not (uid):
            abort(403)
        request.token = token
        return func(*args, **kwargs)
    return verifyTokenWrapper


def generateToken(uid):
    return shake_256(str(uid).encode("UTF-8")).hexdigest(10)\
        + "."\
        + shake_256(
            str(
                time.time()
            ).encode("UTF-8")
        ).hexdigest(10)


def uidFromToken(token):
    uid = tokenDbGet(token)
    return uid if uid else None

# Logs for requests
@app.after_request
def logRequest(response):
    LOG_INFO(f"{request.path}", f" {request.method} ".rjust(6, " "), f" {response.status_code} ")
    return response

def sha(data):
    return sha3_256(str(data).encode("UTF-8")).hexdigest()

# --- --- --- --- ---
# Errors
# --- --- --- --- ---
@app.errorhandler(werkzeug.exceptions.HTTPException)
def errorBasic(e):
    return renderTemplate("error.html", code=e.code, message=e.description), e.code

# --- --- --- --- ---
# Pages
# --- --- --- --- ---
@app.route("/editor/<eid>")
def editorPage(eid):
    return renderTemplate("editor.html")

@app.route("/editor/invalid")
def invalidEditorPage():
    abort(404)

@app.route("/login", methods=["GET", "POST"])
def loginPage():
    if (request.method == "POST"):
        if ("username" in request.form.keys()
        and "password" in request.form.keys()
        ):
            token = login(request.form.get("username"), request.form.get("password"))
            if not (token): return renderTemplate("login.html", error="Login failed: Invalid username or password")
            return renderTemplate("login_forward.html", token=token["token"])
    else: return renderTemplate("login.html")

@app.route("/register", methods=["GET", "POST"])
def registerPage():
    if (request.method == "POST"):
        if ("username" in request.form.keys()
        and "password" in request.form.keys()
        ):
            if not (request.form.get("password")):
                return renderTemplate("login.html", error="Registering failed: Password cannot be empty")
            uid = register(request.form.get("username"), request.form.get("password"))
            if not (uid):
                return renderTemplate("login.html", error="Registering failed: Username is taken")
            token = login(request.form.get("username"), request.form.get("password"))["token"]
            return renderTemplate("login_forward.html", token=token)
    else: return renderTemplate("login.html")

# --- --- --- --- ---
# User API
# --- --- --- --- ---
@app.route("/api/user/create", methods=["POST"])
def apiUserCreate():
    if not (testStruct(request.json, apiUserCreatePacketTemplate)): abort(400)
    uid = register(request.json["username"], request.json["password"])
    if not (uid): return json.dumps({"err" : "Username is taken"})
    return json.dumps({"uid" : uid}), 200

@app.route("/api/user/login", methods=["POST"])
def apiUserLogin():
    if not (testStruct(request.json, apiUserLoginPacketTemplate)): abort(400)
    result = login(request.json["username"], request.json["password"])
    if not (result): abort(403)
    else: return json.dumps(result), 200

@app.route("/api/user/logout", methods=["POST"])
@verifyToken
def apiUserLogout():
    tokenDbExpire(request.token)
    return Response(status=200)

# --- --- --- --- ---
# Editor API
# --- --- --- --- ---
@app.route("/api/editor/create", methods=["POST"])
@verifyToken
def apiEditorCreate():
    if not (testStruct(request.json, apiEditorCreatePacketTemplate)): abort(400)
    return json.dumps({"eid" : dbCreateEditor(
        request.json["title"],
        request.uid,
        request.json["config"]
    )}), 200

@app.route("/api/editor/<eid>/get-details", methods=["GET"])
def apiEditorGetInfo(eid):
    abort(501)

@app.route("/api/editor/<eid>/get-config", methods=["GET"])
def apiEditorGetConfig(eid):
    try:
        eid = int(eid)
    except ValueError:
        abort(400)
    return json.dumps(dbGetEditorConfig(eid))

@app.route("/api/editor/index", methods=["GET"])
def apiEditorGetIndex():
    startIndex = request.args.get("from", default=1,  type=int)
    endIndex   = request.args.get("to",   default=10, type=int)
    if endIndex > 100: endIndex = 100
    abort(501)


# --- --- --- --- ---
# Image API AKA janky repeated code that I don't feel like doing in a less janky and repetitive way either
# --- --- --- --- ---
@app.route("/api/image/upload/category-icon/<eid>/<cidx>", methods=["PUT"])
@verifyToken
def apiImageCategoryIconUpload(eid, cidx):
    try:
        eid = int(eid); cidx = int(cidx)
    except ValueError:
        return json.dumps({"err" : "Invalid index"}), 200

    if not dbCheckOwnership(eid, request.uid): abort(403)

    if not (os.path.exists(f"{DATA_PATH}/images/{eid}")):
        os.makedirs(f"{DATA_PATH}/images/{eid}/{cidx}")

    with open(f"{DATA_PATH}/images/{eid}/{cidx}-icon.png", "wb") as imgFile:
        imgFile.write(request.data)

    if (imgType(f"{DATA_PATH}/images/{eid}/{cidx}-icon.png") != "png"):
        os.remove(f"{DATA_PATH}/images/{eid}/{cidx}-icon.png")

    return Response(status=200)

@app.route("/api/image/upload/image-icon/<eid>/<cidx>/<iidx>", methods=["PUT"])
@verifyToken
def apiImageImageIconUpload(eid, cidx, iidx):
    try:
        eid = int(eid); cidx = int(cidx); iidx = int(iidx)
    except ValueError:
        return json.dumps({"err" : "Invalid index"}), 200

    if not dbCheckOwnership(eid, request.uid): abort(403)

    if not (os.path.exists(f"{DATA_PATH}/images/{eid}/{cidx}")):
        os.makedirs(f"{DATA_PATH}/images/{eid}/{cidx}")

    with open(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}-icon.png", "wb") as imgFile:
        imgFile.write(request.data)

    if (imgType(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}-icon.png") != "png"):
        os.remove(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}-icon.png")

    return Response(status=200)

@app.route("/api/image/upload/image-icon/<eid>/<cidx>/<iidx>/<vidx>", methods=["PUT"])
@verifyToken
def apiImageVariantIconUpload(eid, cidx, iidx, vidx):
    try:
        eid = int(eid); cidx = int(cidx); iidx = int(iidx); vidx = int(vidx)
    except ValueError:
        return json.dumps({"err" : "Invalid index"}), 200

    if not dbCheckOwnership(eid, request.uid): abort(403)

    if not (os.path.exists(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}")):
        os.makedirs(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}")

    with open(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}/{vidx}-icon.png", "wb") as imgFile:
        imgFile.write(request.data)

    if (imgType(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}/{vidx}-icon.png") != "png"):
        os.remove(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}/{vidx}-icon.png")

    return Response(status=200)

@app.route("/api/image/upload/<eid>/<cidx>/<iidx>/<vidx>", methods=["PUT"])
@verifyToken
def apiImageUpload(eid, cidx, iidx, vidx):
    try:
        eid = int(eid); cidx = int(cidx); iidx = int(iidx); vidx = int(vidx)
    except ValueError:
        return json.dumps({"err" : "Invalid index"}), 200

    if not dbCheckOwnership(eid, request.uid): abort(403)

    if not (os.path.exists(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}")):
        os.makedirs(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}")

    with open(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}/{vidx}.png", "wb") as imgFile:
        imgFile.write(request.data)

    if (imgType(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}/{vidx}.png") != "png"):
        os.remove(f"{DATA_PATH}/images/{eid}/{cidx}/{iidx}/{vidx}.png")

    return Response(status=200)

@app.route("/api/image/get/category-icon/<eid>/<cidx>.png")
def apiCategoryIconGet(eid, cidx):
    try:
        eid = int(eid); cidx = int(cidx)
    except ValueError:
        return json.dumps({"err" : "Invalid index"}), 200
    try:
        return sendFile(f"../data/images/{eid}/{cidx}-icon.png")
    except FileNotFoundError:
        try:
            return sendFile(f"../data/images/{eid}/{cidx}/0/0.png")
        except FileNotFoundError:
            abort(404)

@app.route("/api/image/get/image-icon/<eid>/<cidx>/<iidx>.png")
def apiImageIconGet(eid, cidx, iidx):
    try:
        eid = int(eid); cidx = int(cidx); iidx = int(iidx)
    except ValueError:
        return json.dumps({"err" : "Invalid index"}), 200
    try:
        return sendFile(f"../data/images/{eid}/{cidx}/{iidx}-icon.png")
    except FileNotFoundError:
        try:
            return sendFile(f"../data/images/{eid}/{cidx}/{iidx}/0.png")
        except FileNotFoundError:
            abort(404)

@app.route("/api/image/get/variant-icon/<eid>/<cidx>/<iidx>/<vidx>.png")
def apiVariantIconGet(eid, cidx, iidx, vidx):
    try:
        eid = int(eid); cidx = int(cidx); iidx = int(iidx); int(vidx)
    except ValueError:
        return json.dumps({"err" : "Invalid index"}), 200
    try:
        return sendFile(f"../data/images/{eid}/{cidx}/{iidx}/{vidx}-icon.png")
    except FileNotFoundError:
        try:
            return sendFile(f"../data/images/{eid}/{cidx}/{iidx}/{vidx}.png")
        except FileNotFoundError:
            abort(404)

@app.route("/api/image/get/<eid>/<cidx>/<iidx>/<vidx>.png")
def apiImageGet(eid, cidx, iidx, vidx):
    try:
        eid = int(eid); cidx = int(cidx); iidx = int(iidx); vidx = int(vidx)
    except ValueError:
        return json.dumps({"err" : "Invalid index"}), 200
    try:
        return sendFile(f"../data/images/{eid}/{cidx}/{iidx}/{vidx}.png")
    except FileNotFoundError:
        abort(404)


if __name__ == "__main__":
    app.run(HOST, PORT)
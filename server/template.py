from log import LOG_DBUG

class Optional(object):
    def __init__(self, struct):
        self.struct = struct

def testStruct(struct, template, strict=True):
    # struct is a list, test each element against first in template
    if (isinstance(struct, list)):
        return all([testStruct(s, template[0]) for s in struct])

    # struct is a dict, loop inside
    elif (isinstance(struct, dict)):
        l = []
        for s in template.keys():
            # Required key missing, return false
            if (s not in struct.keys() and not isinstance(template[s], Optional)): LOG_DBUG("ReqMiss"); return False
            # Optional key missing, interpret as valid
            elif (s not in struct.keys() and isinstance(template[s], Optional)): l.append(True)
            # Optional key present, recurse
            elif (isinstance(template[s], Optional)): l.append(testStruct(struct[s], template[s].struct))
            # Required key present, recurse
            else: l.append(testStruct(struct[s], template[s]))
        # Strict check on, fail if extra keys in struct
        if (strict and len(struct.keys() - template.keys()) > 0): LOG_DBUG("StrictExtra"); return False
        return all(l)

    # template is a type
    elif (isinstance(template, type)):
        return isinstance(struct, template)

    # No idea how we'd get here
    else:
        return False


apiUserLoginPacketTemplate = {
    "username" : str,
    "password" : str
}

apiUserCreatePacketTemplate = {
    "username" : str,
    "password" : str
}

apiEditorCreatePacketTemplate = {
    "title"  : str,
    "author" : int,
    "config" : {
        "categories" : [
            {
                "title"    : str,
                "nullable" : Optional(bool),
                "movable"  : Optional(bool),
                "images"   : [
                    {
                        "title"    : str,
                        "icon"     : Optional(bool),
                        "variants" : Optional([ str ])
                    }
                ]
            }
        ]
    }
}
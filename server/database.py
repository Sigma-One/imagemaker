import json
import sqlite3 as sql
import time

from log import LOG_DBUG
from log import LOG_WARN

class Error(object):
    def __init__(self, code):
        self.code = code

with sql.connect("../data/database.db") as con:
    globalCur = con.cursor()

    globalCur.execute("CREATE TABLE IF NOT EXISTS editors ( \
                 id     INTEGER PRIMARY KEY AUTOINCREMENT, \
                 title  TEXT, \
                 author INTEGER, \
                 config TEXT \
    )")

    #TODO: EMail maybe?
    globalCur.execute("CREATE TABLE IF NOT EXISTS users ( \
                id      INTEGER PRIMARY KEY AUTOINCREMENT, \
                name    TEXT, \
                pass    TEXT, \
                editors TEXT \
    )")

    globalCur.execute("CREATE TABLE IF NOT EXISTS tokens ( \
                token  TEXT, \
                id     INT, \
                expiry INT \
    )")
    con.commit()

def dbCreateEditor(title, author, config):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        userEditors = dbGetUserEditors(author)
        if (userEditors is None):
            return None
        cur.execute("INSERT INTO editors VALUES (NULL, ?, ?, ?)", (title, author, json.dumps(config)))
        i = cur.lastrowid
        userEditors.append(i)
        cur.execute("UPDATE users SET editors=? WHERE id=?", (json.dumps(userEditors), author))
        con.commit()
        return i

def dbCreateUser(name, password):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        cur.execute("SELECT EXISTS( SELECT id FROM users WHERE name=?)", (name,))
        n = cur.fetchone()
        if (n[0]):
            return None
        cur.execute("INSERT INTO users VALUES (NULL, ?, ?, ?)", (name, password, ""))
        con.commit()
        return cur.lastrowid

def dbVerifyUserLogin(name, password):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        cur.execute("SELECT id, pass FROM users WHERE name=?", (name,))
        try:
            uid, dbPassword = cur.fetchone()
            if (password == dbPassword):
                return uid
        except IndexError:
            return None
        except TypeError:
            return None

def dbGetUserEditors(uid):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        cur.execute("SELECT editors FROM users WHERE id=?", (uid,))
        try:
            try:
                return (json.loads(cur.fetchall()[0][0]))
            except json.JSONDecodeError:
                return []
        except IndexError:
            return None

def dbCheckOwnership(eid, uid):
    print(dbGetUserEditors(uid))
    if (eid in dbGetUserEditors(uid)): return True
    else: return False

def dbGetEditorInfo(eid):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        cur.execute("SELECT * FROM editors WHERE id=?", (eid,))
        try:
            return (cur.fetchall()[0])
        except IndexError:
            return None

def dbGetEditorConfig(eid):
    try:
        return json.loads(dbGetEditorInfo(eid)[3])
    except json.JSONDecodeError:
        return None

def dbGetEditorList(start, end):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        cur.execute("SELECT id, title, author FROM editors WHERE id>? AND id<?", (start, end))
        try:
            return (cur.fetchall())
        except IndexError:
            return None

def tokenDbGet(token):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        cur.execute("SELECT id, expiry FROM tokens WHERE token=?", (token,))
        try:
            uid, expiry = cur.fetchone()
            if (int(time.time()) < int(expiry)): return uid
            else:
                cur.execute("DELETE FROM tokens WHERE token=?", (token,))
                return None
        except IndexError:
            return None
        except TypeError:
            return None

def tokenDbSet(token, uid, expiry=(30*24*60*60)):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        cur.execute("SELECT EXISTS( SELECT id FROM tokens WHERE token=?)", (token,))
        n = cur.fetchone()
        print(n)
        if (n[0]):
            return None
        cur.execute("INSERT INTO tokens VALUES (?, ?, ?)", (token, uid, int(time.time())+expiry))
        con.commit()
        return uid

def tokenDbExpire(token):
    with sql.connect("../data/database.db") as con:
        cur = con.cursor()
        cur.execute("DELETE FROM tokens WHERE token=?", (token,))
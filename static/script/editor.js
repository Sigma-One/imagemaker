let images = [];
let layers = [];
let layerVariants = [];
let layerImages = [];
let imageIcons = [];
let currentCategory = 0;
let variantsVisible = false;
let canvasCtx = null;
let canvas = null;
let crossImage = new Image();
let editorId = 0;

crossImage.src = "/static/editor-assets/cross.png";
crossImage.style.width = "40pt";

let editorApiURI = "/api/editor"
let imageApiURI = "/api/image/get"

let makerJson;

function makeSelectionButton(title, imgIdx, catIdx) {
    // Generate a selection button for an element
    let button = document.createElement("button");
    button.onclick = (ev) => setImage(catIdx, imgIdx);
    //button.innerText = title;
    let image;
    if (imgIdx > -1) {
        image = imageIcons[catIdx][imgIdx];
        button.appendChild(image);
    }
    else {
        button.appendChild(crossImage);
    }
    button.className = "imageButton";
    return button
}

function makeVariantButton(title, varIdx, imgIdx, catIdx) {
    // Generate a selection button for a variant
    let button = document.createElement("button");
    button.onclick = (ev) => {
        layerVariants[catIdx][imgIdx] = varIdx;
        layers[catIdx][0] = images[catIdx][imgIdx][varIdx];
        refresh();
    };
    //button.innerText = title;
    button.className = "variantButton";
    return button
}

function makeCategoryTab(title, catIdx) {
    // Generate a category tab thing
    let tab = document.createElement("button");
    tab.className = "categoryTab";
    tab.onclick = (ev) => setCategory(catIdx);
    tab.id = `ct${catIdx}`;
    //tab.innerText = title;
    let i = new Image()
    i.src = `${imageApiURI}/category-icon/${editorId}/${catIdx}.png`;
    i.style.width = "40pt";
    tab.appendChild(i);
    return tab;
}

function makeCategoryContent(catIdx) {
    // Generate the category content
    let content = document.createElement("div");
    content.id = `c${catIdx}`;
    if (catIdx !== 0) {
        content.style.display = "none";
    }
    else {
        content.style.display = "block";
    }
    content.className = "categoryContent"
    return content;
}

function makeVariantContent(imgIdx, catIdx) {
    // Generate the variant panel
    let content = document.createElement("div");
    content.id = `v${imgIdx}c${catIdx}`;
    content.style.display = "none";
    content.className = "variantPanel"
    return content;
}

window.onload = function() {
    // Get editor ID
    editorId = document.location.href.substring(document.location.href.lastIndexOf("/")+1, document.location.href.length);
    let imageURL = imageApiURI + "/" + editorId;
    editorApiURI += "/" + editorId;

    // Fetch JSON
    loadJson(editorApiURI + "/get-config").then((result) => {
        makerJson = result;
        if (makerJson === 1) {
            window.location.href = window.location.pathname = "invalid";
            return;
        }

        // Get canvas and category div
        canvas = document.getElementById("editor");
        canvasCtx = canvas.getContext("2d");
        let categoriesDiv = document.getElementById("categoryContents");
        let categoriesTabDiv = document.getElementById("categoryTabs");
        let variantPanel = document.getElementById("variantPanel");

        // Loop through categories
        makerJson.categories.forEach((category, cidx) => {
            // Add an image set
            images.push([]);
            imageIcons.push([]);
            layerVariants.push([]);
            layerImages.push(0);
            let categoryTab = makeCategoryTab(category.title, cidx);
            let categoryDiv = makeCategoryContent(cidx);

            // Category is nullable, add a no image button
            if (category.nullable) {
                categoryDiv.appendChild(
                    makeSelectionButton(
                        "None",
                        -1,
                        cidx
                    )
                );
            }

            // Loop through and load images
            category.images.forEach((image, iidx) => {
                let variants = !(typeof image == 'string');
                if (variants) {
                    if (!image["variants"]) {
                        variants = false;
                    }
                }
                layerVariants.last().push([0])
                images.last().push([]);
                if (variants) {
                    let panel = makeVariantContent(iidx, cidx);
                    image["variants"].forEach((variant, vidx) => {
                        let i = new Image()
                        i.src = `${imageURL}/${cidx}/${iidx}/${vidx}.png`;
                        images.last().last().push(i);
                        let btn = makeVariantButton(
                            vidx,
                            vidx,
                            iidx,
                            cidx
                        );
                        if (variant === "icon") {
                            let ic = new Image();
                            ic.src = `${imageApiURI}/variant-icon/${editorId}/${cidx}/${iidx}/${vidx}.png`;
                            ic.style.width = "40pt";
                            btn.appendChild(ic);
                        }
                        else {
                            let d = document.createElement("div");
                            d.className = "colourCircle";
                            d.style.background = variant;
                            btn.appendChild(d);
                        }
                        panel.appendChild(btn);
                    });
                    variantPanel.appendChild(
                        panel
                    );
                }
                else {
                    let i = new Image();
                    i.src = `${imageURL}/${cidx}/${iidx}/0.png`;
                    images.last().last().push(i);
                }

                let i = new Image();
                if (image["icon"]) {
                    i.src = `${imageApiURI}/image-icon/${editorId}/${cidx}/${iidx}.png`;
                }
                else {
                    i.src = images[cidx][iidx][0].src;
                }
                i.style.width = "40pt";
                imageIcons.last().push(i);
                categoryDiv.appendChild(
                    makeSelectionButton(
                        variants? image["title"] : image,
                        iidx,
                        cidx
                    )
                );
            });

            // If category is nullable, default to null, else first image
            if (category.nullable) {
                layers.push([null, 0, 0, !!category.movable, 0]);
            }
            else {
                layers.push([images[cidx][0][0], 0, 0, !!category.movable, 0]);
                layers.last()[0].onload = function () {
                    refresh();
                }
            }
            categoriesTabDiv.appendChild(categoryTab);
            categoriesDiv.appendChild(categoryDiv);
        });
    });
    document.getElementById("downloadButton").addEventListener("click", download, false);
}

function download() {
    let dlURL = canvas.toDataURL('image/png');
    document.getElementById("downloadButton").href = dlURL.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
}

function setCategory(catIdx) {
    if (variantsVisible) {
        toggleVariants();
    }
    let allCats = document.getElementsByClassName("categoryContent");
    let allTabs = document.getElementsByClassName("categoryTab");
    for (let i = 0; i < allCats.length; i++) {
        allCats[i].style.display = "none";
        allTabs[i].classList.remove("selected");
    }
    let category = document.getElementById(`c${catIdx}`);
    let tab = document.getElementById(`ct${catIdx}`);
    if (layers[catIdx][3]) {
        document.getElementById("transformContainer").style.opacity = "255";
    }
    else {
        document.getElementById("transformContainer").style.opacity = "0";
    }
    category.style.display = "block";
    tab.classList.add("selected");
    currentCategory = catIdx;
}

function toggleVariants() {
    let l;
    try {
        l = images[currentCategory][layerImages[currentCategory]].length;
    }
    catch (e) {
        if (e.name === "TypeError") { l = 0; }
        else { console.log(e); }
    }
    if (l > 1) {
        let allVars = document.getElementsByClassName("variantPanel");
        for (let i = 0; i < allVars.length; i++) {
            allVars[i].style.display = "none";
        }
        if (!variantsVisible) {
            document.getElementById(`c${currentCategory}`).style.display = "none";
            if (layerImages[currentCategory] > -1) {
                try {
                    document.getElementById(`v${layerImages[currentCategory]}c${currentCategory}`).style.display = "block";
                } catch (TypeError) {
                }
            }
        } else {
            document.getElementById(`c${currentCategory}`).style.display = "block";
        }
        variantsVisible = !variantsVisible;
    }
}

function setImage(layer, selection) {
    layerImages[layer] = selection;
    if (selection > -1) {
        layers[layer][0] = images[layer][selection][layerVariants[layer][selection]];
    }
    // Special selection index -1 sets layer to null
    // Null layers are not rendered
    else {
        layers[layer][0] = null;
    }
    refresh();
    toggleVariants();
    toggleVariants();
}

function rotateLayer(rot) {
    if (layers[currentCategory][3]) {
        layers[currentCategory][4] += rot;
        refresh();
    }
}

function moveLayer(x, y) {
    if (layers[currentCategory][3]) {
        layers[currentCategory][1] += x;
        layers[currentCategory][2] += y;
        refresh();
    }
}

function refresh() {
    // Clear canvas and draw each layer (if it's not null)
    canvasCtx.clearRect(0, 0, canvas.width, canvas.height);
    for (let i in layers) {
        if (layers[i][0] !== null && layers[i][0] !== undefined) {
            canvasCtx.translate(canvas.width/2, canvas.height/2);
            canvasCtx.translate(layers[i][1], layers[i][2]);
            canvasCtx.rotate(layers[i][4] * 3.14/180);
            canvasCtx.drawImage(layers[i][0], -canvas.width/2, -canvas.height/2);
            canvasCtx.rotate(-layers[i][4] * 3.14/180);
            canvasCtx.translate(-layers[i][1], -layers[i][2]);
            canvasCtx.translate(-canvas.width/2, -canvas.height/2);
        }
    }
}
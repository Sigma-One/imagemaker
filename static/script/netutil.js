async function loadJson(url) {
    const response = await fetch(url);
    if (!response.ok) {
        console.log("JSON fetch failed");
        return 1;
    }

    try {
        return await response.json();
    }
    catch (e) {
        return 1;
    }
}